package org.bonitasoft.engine.authentication.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.bonitasoft.engine.authentication.AuthenticationConstants;
import org.bonitasoft.engine.authentication.GenericAuthenticationService;
import org.bonitasoft.engine.commons.LogUtil;
import org.bonitasoft.engine.identity.IdentityService;
import org.bonitasoft.engine.identity.SUserNotFoundException;
import org.bonitasoft.engine.identity.model.SUser;
import org.bonitasoft.engine.log.technical.TechnicalLogSeverity;
import org.bonitasoft.engine.log.technical.TechnicalLoggerService;

/**
 * @author Elias Ricken de Medeiros
 * @author Matthieu Chaffotte
 * @author Hongwen Zang
 * @author Julien Reboul
 * @author Celine Souchet
 */
public class AuthenticationServiceImpl implements GenericAuthenticationService {

    private  IdentityService identityService;

    private  TechnicalLoggerService logger;
    
	private String domain; 
	private String ldapHost; 
	private String searchBase;     


	/*public AuthenticationServiceImpl() {
		Properties ldap = new Properties();
		InputStream input = null;
		try{
			input = new FileInputStream("C:/BonitaBPMCommunity-7.1.4/workspace/tomcat/conf/nsiLdap.properties");
			ldap.load(input);
			this.domain = ldap.getProperty("domain"); 
		    this.ldapHost = ldap.getProperty("ldapHost"); 
		    this.searchBase = ldap.getProperty("searchBase"); // BaseDN
		}catch (IOException ex){
			ex.printStackTrace();
		}	    
	  } */ 


    public AuthenticationServiceImpl(final IdentityService identityService, final TechnicalLoggerService logger) {
        this.identityService = identityService;
        this.logger = logger;
        Properties ldap = new Properties();
		InputStream input = null;
		try{
			input = new FileInputStream("C:/BonitaBPMCommunity-7.1.4/workspace/tomcat/conf/nsiLdap.properties");
			ldap.load(input);
			this.domain = ldap.getProperty("domain"); 
		    this.ldapHost = ldap.getProperty("ldapHost"); 
		    this.searchBase = ldap.getProperty("searchBase"); // BaseDN
		}catch (IOException ex){
			ex.printStackTrace();
		}
    }

    /**
     * @see org.bonitasoft.engine.authentication.GenericAuthenticationService#checkUserCredentials(java.util.Map)
     */
    @Override
    public String checkUserCredentials(Map<String, Serializable> credentials) {
        final String methodName = "checkUserCredentials";
        try {
            final String password = String.valueOf(credentials.get(AuthenticationConstants.BASIC_PASSWORD));
            final String userName = String.valueOf(credentials.get(AuthenticationConstants.BASIC_USERNAME));
            if (logger.isLoggable(this.getClass(), TechnicalLogSeverity.TRACE)) {
                logger.log(this.getClass(), TechnicalLogSeverity.TRACE, LogUtil.getLogBeforeMethod(this.getClass(), methodName));
            }
            final SUser user = identityService.getUserByUserName(userName);
            /**
             * B�squeda en Ldap
             **/
            
            boolean Encontrado;
            Encontrado = authenticate(userName,password);
            if(Encontrado)
            {
            	return userName;
            }
            
            if (identityService.chechCredentials(user, password)) {
                if (logger.isLoggable(this.getClass(), TechnicalLogSeverity.TRACE)) {
                    logger.log(this.getClass(), TechnicalLogSeverity.TRACE, LogUtil.getLogAfterMethod(this.getClass(), methodName));
                }
                return userName;
            }
            if (logger.isLoggable(this.getClass(), TechnicalLogSeverity.TRACE)) {
                logger.log(this.getClass(), TechnicalLogSeverity.TRACE, LogUtil.getLogAfterMethod(this.getClass(), methodName));
            }
        } catch (final SUserNotFoundException sunfe) {
            if (logger.isLoggable(this.getClass(), TechnicalLogSeverity.TRACE)) {
                logger.log(this.getClass(), TechnicalLogSeverity.TRACE, LogUtil.getLogOnExceptionMethod(this.getClass(), methodName, sunfe));
            }
        }
        return null;
    }
    
    public boolean authenticate(String user, String pass) { 
        String returnedAtts[] ={  }; 
        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";
        // Create the search controls 
        SearchControls searchCtls = new SearchControls();
        searchCtls.setReturningAttributes(returnedAtts); 
        // Specify the search scope 
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        Hashtable<String,String> env = new Hashtable<String,String>(); 
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory"); 
        env.put(Context.PROVIDER_URL, ldapHost);
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); 
        env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
        env.put(Context.SECURITY_CREDENTIALS, pass);
        LdapContext ctxGC = null; 

        try { 
            // This is the actual Authentication piece. Will throw javax.naming.AuthenticationException
            // if the users password is not correct. Other exceptions may include IO (server not found) etc. 
            ctxGC = new InitialLdapContext(env, null);       
            // Now try a simple search and get some attributes as defined in returnedAtts 
            NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase, searchFilter, searchCtls); 
            if (answer.hasMoreElements()) { 
            		return true;	                } 
             ctxGC.close();  // Close and clean up 	            
        } catch (NamingException nex) { 
            nex.printStackTrace(); 
        } catch (Exception ex) { 
            ex.printStackTrace(); 
        } 
 	        return false; 
    } 

}
