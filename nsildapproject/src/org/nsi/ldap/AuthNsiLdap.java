package org.nsi.ldap;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang3.StringUtils;

//import javax.servlet.ServletException;

import org.bonitasoft.console.common.server.auth.AuthenticationFailedException;
import org.bonitasoft.console.common.server.auth.AuthenticationManager;
import org.bonitasoft.console.common.server.auth.ConsumerNotFoundException;
import org.bonitasoft.console.common.server.login.HttpServletRequestAccessor;
import org.bonitasoft.console.common.server.login.datastore.Credentials;
import org.bonitasoft.engine.authentication.AuthenticationConstants;


public class AuthNsiLdap implements AuthenticationManager {
	private static final Logger LOGGER = Logger.getLogger(AuthNsiLdap.class.getName());
	
	private String domain; 
	private String ldapHost; 
	private String searchBase; 

	public AuthNsiLdap() { 
		Properties ldap = new Properties();
		InputStream input = null;
		try{
			input = new FileInputStream("C:/BonitaBPMCommunity-7.1.4/workspace/tomcat/webapps/bonita/WEB-INF/nsiLdap.properties");
			ldap.load(input);
			this.domain = ldap.getProperty("domain"); 
		    this.ldapHost = ldap.getProperty("ldapHost"); 
		    this.searchBase = ldap.getProperty("searchBase"); // BaseDN
		}catch (IOException ex){
			ex.printStackTrace();
		}	
	  }  

	public AuthNsiLdap(String domain, String host, String dn)
	  { 
	    this.domain = domain;
	    this.ldapHost = host; 
	    this.searchBase = dn; 
	  } 
	
	@Override
    public String getLoginPageURL(final HttpServletRequestAccessor request, final String redirectURL) {
        final StringBuilder url = new StringBuilder();
        String context = request.asHttpServletRequest().getContextPath();
        final String servletPath = request.asHttpServletRequest().getServletPath();
        if (StringUtils.isNotBlank(servletPath) && servletPath.startsWith("/mobile")) {
            context += "/mobile";
        }
        url.append(context).append(AuthenticationManager.LOGIN_PAGE).append("?");
        if (request.getTenantId() != null) {
            url.append(AuthenticationManager.TENANT).append("=").append(request.getTenantId()).append("&");
        }
        url.append(AuthenticationManager.REDIRECT_URL).append("=").append(redirectURL);
        return url.toString();
    }
	
	public Map<String, Serializable> authenticate(final HttpServletRequestAccessor requestAccesor, final Credentials credentials) throws AuthenticationFailedException{
		boolean Encontrado;
		Map<String, Serializable> credenciales = new HashMap<String, Serializable>();
		credenciales.put(AuthenticationConstants.BASIC_USERNAME, requestAccesor.getUsername());
		credenciales.put(AuthenticationConstants.BASIC_PASSWORD, requestAccesor.getPassword());
		LOGGER.log(Level.SEVERE, "Proceder a buscar usario en Ldap");
		LOGGER.log(Level.SEVERE, "Username: " + requestAccesor.getUsername().toString() + " Password: " + requestAccesor.getPassword().toString());
		Encontrado = authenticate(requestAccesor.getUsername().toString(), requestAccesor.getPassword().toString());
		
		if(LOGGER.isLoggable(Level.FINE)){
			LOGGER.log(Level.FINE, "Username: " + requestAccesor.getUsername().toString() + " Password: " + requestAccesor.getPassword().toString());
		}
		if(Encontrado)
		{
			LOGGER.log(Level.SEVERE, "Se encontr� el usuario");							
		}
		else
		{
			LOGGER.log(Level.SEVERE, "No se encontr� el usuario");
			throw new AuthenticationFailedException("Usuario no encontrado");
		}
		return credenciales;
	}

	@Override
	public String getLogoutPageURL(HttpServletRequestAccessor arg0, String arg1)
			throws ConsumerNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean authenticate(String user, String pass) { 
        String returnedAtts[] ={ "sn", "givenName", "name", "userPrincipalName", "displayName", "memberOf","sAMAccountName" }; 
        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";
        // Create the search controls 
        SearchControls searchCtls = new SearchControls();
        searchCtls.setReturningAttributes(returnedAtts); 
        // Specify the search scope 
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        Hashtable<String,String> env = new Hashtable<String,String>(); 
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory"); 
        env.put(Context.PROVIDER_URL, ldapHost);
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); 
        env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
        env.put(Context.SECURITY_CREDENTIALS, pass);
        LdapContext ctxGC = null; 

        try { 
            // This is the actual Authentication piece. Will throw javax.naming.AuthenticationException
            // if the users password is not correct. Other exceptions may include IO (server not found) etc. 
            ctxGC = new InitialLdapContext(env, null);       
            // Now try a simple search and get some attributes as defined in returnedAtts 
            NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase, searchFilter, searchCtls); 
            while (answer.hasMoreElements()) { 
                SearchResult sr = (SearchResult) answer.next(); 
                Attributes attrs = sr.getAttributes(); 
                Map<String, Object> amap = null; 
                if (attrs != null) { 
                    amap = new HashMap<String,Object>(); 
                    NamingEnumeration<?> ne = attrs.getAll(); 
                    while (ne.hasMore()) { 
                        Attribute attr = (Attribute) ne.next(); 
                        if (attr.size() == 1) { 
                            amap.put(attr.getID(), attr.get()); 
                        } else { 
                            HashSet<String> s = new HashSet<String>(); 
                            NamingEnumeration n =  attr.getAll(); 
                            while (n.hasMoreElements()) { 
                                s.add((String)n.nextElement()); 
                            } 
                            amap.put(attr.getID(), s); 
                        } 
                    } 
                    ne.close(); 
                } 
                ctxGC.close();  // Close and clean up 
                return true; 
            } 
        } catch (NamingException nex) { 
            nex.printStackTrace(); 
        } catch (Exception ex) { 
            ex.printStackTrace(); 
        } 
 	        return false; 
    } 	

}
