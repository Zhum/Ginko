package org.java.ldap;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;


public class LdapSuccess {
	private String domain; 
	private String ldapHost; 
	private String searchBase;     


	public LdapSuccess() {
		Properties ldap = new Properties();
		InputStream input = null;
		try{
			input = new FileInputStream("C:/Users/k2service/Desktop/nsiLdap.properties");
			ldap.load(input);
			this.domain = ldap.getProperty("domain"); 
		    this.ldapHost = ldap.getProperty("ldapHost"); 
		    this.searchBase = ldap.getProperty("searchBase"); // BaseDN
		}catch (IOException ex){
			ex.printStackTrace();
		}	    
	  }  


	
	public boolean authenticate(String user, String pass) { 
	        String returnedAtts[] ={  }; 
	        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";
	        // Create the search controls 
	        SearchControls searchCtls = new SearchControls();
	        searchCtls.setReturningAttributes(returnedAtts); 
	        // Specify the search scope 
	        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	        Hashtable<String,String> env = new Hashtable<String,String>(); 
	        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory"); 
	        env.put(Context.PROVIDER_URL, ldapHost);
	        env.put(Context.SECURITY_AUTHENTICATION, "simple"); 
	        env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
	        env.put(Context.SECURITY_CREDENTIALS, pass);
	        LdapContext ctxGC = null; 

	        try { 
	            // This is the actual Authentication piece. Will throw javax.naming.AuthenticationException
	            // if the users password is not correct. Other exceptions may include IO (server not found) etc. 
	            ctxGC = new InitialLdapContext(env, null);       
	            // Now try a simple search and get some attributes as defined in returnedAtts 
	            NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase, searchFilter, searchCtls); 
	            if (answer.hasMoreElements()) { 
	            		return true;	                } 
	             ctxGC.close();  // Close and clean up 	            
	        } catch (NamingException nex) { 
	            nex.printStackTrace(); 
	        } catch (Exception ex) { 
	            ex.printStackTrace(); 
	        } 
     	        return false; 
	    } 
	public static void main(String[] args) {
		LdapSuccess LdapSuccess = new LdapSuccess(); 
   	        // Test bad password 
	        //System.out.println("Testing bad password..."); 
	        //Map<String,Object> attrs = adAuthenticator.authenticate("SOMEUSER","badpassword"); 
	        // Test good password 
	        System.out.println("Testing good password..."); 
	        boolean attrs = LdapSuccess.authenticate("mzhu","Pa$$word"); 
            if (attrs) { 
            	System.out.println("Se valid�"); 
	        } else { 
	            System.out.println("Usuario no se pudo validar"); 
	        } 
	    } 	

}